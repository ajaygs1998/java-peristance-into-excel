package com.search.campaign;

public class Campaign {
    private String name;
    private String duration;
    private String budget;
    private String roi;

    // Constructor
    public Campaign(String name, String duration, String budget, String roi) {
        this.name = name;
        this.duration = duration;
        this.budget = budget;
        this.roi = roi;
    }

    // Getters
    public String getName() { return name; }
    public String getDuration() { return duration; }
    public String getBudget() { return budget; }
    public String getRoi() { return roi; }
}
