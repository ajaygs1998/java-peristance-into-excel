package com.search.campaign;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ExcelCampaignWriter {

    public static void main(String[] args) {
        List<Campaign> campaigns = new ArrayList<>();
        // Example data - replace with actual campaign data
        campaigns.add(new Campaign("Campaign A", "30 days", "$1000", "20%"));
        campaigns.add(new Campaign("Campaign B", "45 days", "$1500", "30%"));

        writeCampaignDataToExcel(campaigns, "CampaignData1.xlsx");
    }

    private static void writeCampaignDataToExcel(List<Campaign> campaigns, String fileName) {
        try (Workbook workbook = new XSSFWorkbook()) {
            Sheet sheet = workbook.createSheet("Campaigns");

            // Create header row
            String[] columns = {"Campaign Name", "Duration", "Budget", "ROI"};
            Row headerRow = sheet.createRow(0);
            for (int i = 0; i < columns.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(columns[i]);
            }

            // Fill data
            int rowNum = 1;
            for (Campaign campaign : campaigns) {
                Row row = sheet.createRow(rowNum++);
                row.createCell(0).setCellValue(campaign.getName());
                row.createCell(1).setCellValue(campaign.getDuration());
                row.createCell(2).setCellValue(campaign.getBudget());
                row.createCell(3).setCellValue(campaign.getRoi());
            }

            // Auto-size columns
            for (int i = 0; i < columns.length; i++) {
                sheet.autoSizeColumn(i);
            }

            // Write to file
            try (FileOutputStream fileOut = new FileOutputStream(fileName)) {
                workbook.write(fileOut);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
