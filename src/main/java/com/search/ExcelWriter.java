package com.search;

import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.io.FileOutputStream;
import java.io.IOException;

public class ExcelWriter {

    public static void main(String[] args) {
        // Create a Workbook
        Workbook workbook = new XSSFWorkbook(); // new HSSFWorkbook() for generating `.xls` file

        /* CreationHelper helps us create instances of various things like DataFormat,
        Hyperlink, RichTextString etc, in a format (HSSF, XSSF) independent way */
        CreationHelper createHelper = workbook.getCreationHelper();

        // Create a Sheet
        Sheet sheet = workbook.createSheet("Campaign Data");

        // Create a Row
        Row headerRow = sheet.createRow(0);

        // Creating header
        String[] columns = {"Campaign Name", "Duration", "Budget", "ROI"};
        for(int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.createCell(i);
            cell.setCellValue(columns[i]);
        }

        // Create Other rows and cells with campaign data
        int rowNum = 1;
        // Suppose this is your campaign data
        String[][] campaignData = {
                {"Campaign A", "30 days", "$1000", "20%"},
                {"Campaign B", "45 days", "$1500", "30%"}
        };

        for (String[] campaign : campaignData) {
            Row row = sheet.createRow(rowNum++);

            row.createCell(0).setCellValue(campaign[0]);
            row.createCell(1).setCellValue(campaign[1]);
            row.createCell(2).setCellValue(campaign[2]);
            row.createCell(3).setCellValue(campaign[3]);
        }

        // Resize all columns to fit the content size
        for(int i = 0; i < columns.length; i++) {
            sheet.autoSizeColumn(i);
        }

        // Write the output to a file
        try (FileOutputStream fileOut = new FileOutputStream("CampaignData.xlsx")) {
            workbook.write(fileOut);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            workbook.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
